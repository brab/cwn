open Xmltree
open Cwn

let tree = "test.xml" |> to_source |> to_input |> to_xmltree

let t1 () =
  let tree2 =
    Element("document", [
      Element("firstpart", [
        Data "firstdata";
        Element("subdiv", [
          Data "hello"
        ])
      ]);
      Data "enddata"
    ])
  in
  assert (tree = tree2)

let t2 () =
  let sub = get_children_with_tag "firstpart" tree in
  let tree3 = [Data "firstdata"; Element("subdiv", [Data "hello"])] in
  assert(sub = tree3)

let t3 () =
  assert (get_children (Data "Hello") = []);
  assert (get_children (Element("X", [Data "Y"])) = [Data "Y"])

let t4 () =
  assert (get_data_with_tag "X" [Element("X", [Data "Y"])] = ("Y", []));
  try
    let _ = get_data_with_tag "X" [Data "Hello"] in
    assert false
  with Failure _ -> ()

let t5 () =
  let dirty_tree =
    Element("e", [
      Data "\n   ";
      Element("f", [
        Data "\n  ";
        Data "hello"
      ])
    ])
  in
  assert (sanitise dirty_tree = Element("e", [Element("f", [Data "hello"])]))

let cwntree = "test_cwn.xml" |> to_clean_xmltree

let t6 () =
  let cwn = cwntree |> to_cwn in
  let cwn2 =
    { date = "2000.10.20";
      previous = "2000.10.10";
      next = "2000.10.30";
      date_text = "Date as text";
      extra_head_opt = Some "Extra text";
      entries = [
        { title = "Title of the entry";
          url_opt = Some "https://myurl.com";
          content = [
            ("Bob says", "Hello everyone");
            ("Everyone replies", "Hello Bob")
          ]
        };
        { title = "Title of the second entry";
          url_opt = None;
          content = [("Alice says", "Glad to be alone")]
        }
      ]
    }
  in
  assert (cwn = cwn2)

let read_lines in_channel =
  let rec r lines =
    try
      let l = input_line in_channel in
      r (l :: lines)
    with End_of_file -> List.rev lines
  in
  r []

let without_empty_lines lines =
  let rec wel nl = function
    | [] -> List.rev nl
    | l :: r -> if l = "" then wel nl r else wel (l :: nl) r
  in
  wel [] lines

let t7 () =
  let cwn = cwntree |> to_cwn in
  export_as_orgmode cwn "test.org";
  let org = open_in "test.org" in
  let l = without_empty_lines (read_lines org) in
  let l2 = [
    "#+OPTIONS: ^:nil";
    "#+OPTIONS: html-postamble:nil";
    "#+OPTIONS: num:nil";
    "#+OPTIONS: toc:nil";
    "#+OPTIONS: author:nil";
    "#+HTML_HEAD: <style type=\"text/css\">.entryblock { font-family: monospace } #table-of-contents h2 { display: none } .authorname { text-align: right }</style>";
    "[[http://alan.petitepomme.net/cwn/2000.10.10.html][Previous Week]] [[http://alan.petitepomme.net/cwn/2000.10.20.html][Up]] [[http://alan.petitepomme.net/cwn/2000.10.30.html][Next Week]]";
    "Hello";
    "Here is the latest OCaml Weekly News, for the week of Date as text.";
    "Extra text";
    "#+TOC: headlines 2";
    "-----";
    "* <<1>>Title of the entry";
    ":PROPERTIES:";
    ":CUSTOM_ID: 1";
    ":END:";
    "Archive: https://myurl.com";
    "/Bob says:/";
    "#+BEGIN_entryblock";
    "Hello everyone";
    "#+END_entryblock";
    "/Everyone replies:/";
    "#+BEGIN_entryblock";
    "Hello Bob";
    "#+END_entryblock";
    "-----";
    "* <<2>>Title of the second entry";
    ":PROPERTIES:";
    ":CUSTOM_ID: 2";
    ":END:";
    "/Alice says:/";
    "#+BEGIN_entryblock";
    "Glad to be alone";
    "#+END_entryblock";
    "-----";
    "*Old CWN*";
    "If you happen to miss a CWN, you can [[mailto:alan.schmitt@polytechnique.org][send me a message]] and I'll mail it to you, or go take a look at [[http://alan.petitepomme.net/cwn/][the archive]] or the [[http://alan.petitepomme.net/cwn/cwn.rss][RSS feed of the archives]].";
    "If you also wish to receive it every week by mail, you may subscribe [[http://lists.idyll.org/listinfo/caml-news-weekly/][online]].";
    "-----";
    "#+BEGIN_authorname";
    "[[http://alan.petitepomme.net/][Alan Schmitt]]";
    "#+END_authorname"
  ] in
  assert (l = l2)

let t8 () =
  let cwn = cwntree |> to_cwn in
  export_as_rss cwn "test.rss";
  let rss = open_in "test.rss" in
  let l = without_empty_lines (read_lines rss) in
  let l2 = [
    "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
    "<rss version=\"2.0\">";
    "<channel>";
    "<title>OCaml Weekly News</title>";
    "<link>http://alan.petitepomme.net/cwn/</link>";
    "<description>News frow the OCaml list</description>";
    "<item>";
    "  <title>OCaml Weekly News, 20 Oct 2000</title>";
    "  <pubDate>20 Oct 2000 12:00 GMT</pubDate>";
    "  <link>http://alan.petitepomme.net/cwn/2000.10.20.html</link>";
    "  <guid>http://alan.petitepomme.net/cwn/2000.10.20.html</guid>";
    "  <description><ol><li><a href=\"http://alan.petitepomme.net/cwn/2000.10.20.html#1\">Title of the entry</a></li><li><a href=\"http://alan.petitepomme.net/cwn/2000.10.20.html#2\">Title of the second entry</a></li></ol></description>";
    "</item>";
    "</channel>";
    "</rss>"
  ] in
  assert (l = l2)

let () =
  let test_func i test =
    try
      test();
      Printf.printf "Success %d\n" (i+1)
    with _ -> Printf.printf "Failure %d\n" (i+1)
  in
  let tests = [t1; t2; t3; t4; t5; t6; t7; t8] in
  List.iteri test_func tests