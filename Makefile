.PHONY: all test clean

all: main

main: src/main.ml src/xmltree.ml src/cwn.ml
	mkdir -p target
	ocamlbuild -use-ocamlfind -pkgs str,xmlm src/main.native
	mv main.native target/main

test: test/test.ml test/test.xml test/test_cwn.xml
	ocamlbuild -use-ocamlfind -pkgs str,xmlm -I src test/test.native
	mv test.native test/test

clean:
	rm -rf target
	rm -rf _build
	rm -f test/test test/test.org test/test.rss