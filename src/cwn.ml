type cwn_entry =
  { title: string;
    url_opt: string option;
    content: (string * string) list;
  }

type cwn =
  { date: string;
    previous: string;
    next: string;
    date_text: string;
    extra_head_opt: string option;
    entries: cwn_entry list;
  }

open Xmltree

(* ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== *)

let to_cwn xmltree =
  (* Header parsing *)
  let header = get_children_with_tag "cwn_head" xmltree in
  let date, header = get_data_with_tag "cwn_date" header in
  let previous, header = get_data_with_tag "cwn_prev" header in
  let next, header = get_data_with_tag "cwn_next" header in
  let date_text, header = get_data_with_tag "cwn_date_text" header in
  let extra_head_opt =
    try
      let e = fst (get_data_with_tag "cwn_extra_head" header) in
      if e = "" then None else Some e
    with Failure _ -> None
  in

  (* Entries parsing *)
  let entries = get_children_with_tag "cwn_body" xmltree in
  let create_entry entry =
    let entry_children = get_children entry in
    let title, entry_children = get_data_with_tag "cwn_title" entry_children in
    let url_opt, entry_children =
      let f (a, b) = (Some a, b) in
      try f (get_data_with_tag "cwn_url" entry_children)
      with Failure _ -> (None, entry_children)
    in

    let rec process_messages acc = function
      | [] -> List.rev acc
      | messages ->
        let who, messages = get_data_with_tag "cwn_who" messages in
        let what, messages = get_data_with_tag "cwn_what" messages in
        process_messages ((who, what) :: acc) messages
    in
    let content = process_messages [] entry_children in
    { title; url_opt; content }
  in 
  let entries = List.map create_entry entries in

  (* Final object *)
  { date; previous; next; date_text; extra_head_opt; entries }

(* ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== *)

let export_as_orgmode cwn filename =
  (* Function to append things to the output *)
  let append sref s = sref := !sref ^ s in
  let out = ref ("#+OPTIONS: ^:nil\n#+OPTIONS: html-postamble:nil\n"
    ^ "#+OPTIONS: num:nil\n#+OPTIONS: toc:nil\n#+OPTIONS: author:nil\n") in
  
  append out
    ("#+HTML_HEAD: <style type=\"text/css\">#table-of-contents h2 { display: none } "
    ^ ".title { display: none } .authorname { text-align: right }</style>\n"
    ^ "#+HTML_HEAD: <style type=\"text/css\">.outline-2 {border-top: 1px solid black;}</style>\n");

  append out "#+TITLE: OCaml Weekly News\n";

  append out
    ("[[https://alan.petitepomme.net/cwn/" ^ cwn.previous ^ ".html][Previous Week]] "
    ^ "[[https://alan.petitepomme.net/cwn/index.html][Up]] "
    ^ "[[https://alan.petitepomme.net/cwn/" ^ cwn.next ^ ".html][Next Week]]\n\n");

  append out ("Hello\n\nHere is the latest OCaml Weekly News, for the week of "
    ^ cwn.date_text ^ ".\n\n");

  let () =
    match cwn.extra_head_opt with
    | None -> ()
    | Some eh -> append out (eh ^ "\n\n")
  in

  append out "#+TOC: headlines 1\n";

  (* Now writing the whole entries one by one *)
  let replace_markdown_links s =
    let r = Str.regexp "\\[\\([^][]*\\)\\](<?\\(http[^()<>]*\\)>?)" in
    Str.global_replace r "[[\\2][\\1]]" s
  in
  let write_entry_content i entry =
    append out "\n\n";
    append out (Printf.sprintf
      "* %s\n:PROPERTIES:\n:CUSTOM_ID: %d\n:END:\n" entry.title (i+1));
    let () =
      match entry.url_opt with
      | None -> ()
      | Some u ->
        append out (Printf.sprintf "Archive: %s\n\n" u)
    in
    let f (who, what) =
      append out (Printf.sprintf
        "** %s\n\n%s\n\n" who (replace_markdown_links what))
    in
    List.iter f entry.content
  in
  List.iteri write_entry_content cwn.entries;

  (* Footer *)
  append out ("\n\n* Old CWN\n:PROPERTIES:\n:UNNUMBERED: t\n:END:\n\n"
    ^ "If you happen to miss a CWN, you can [[mailto:alan.schmitt@"
    ^ "polytechnique.org][send me a message]] and I'll mail it to you, or go"
    ^ " take a look at [[https://alan.petitepomme.net/cwn/][the archive]] or "
    ^ "the [[https://alan.petitepomme.net/cwn/cwn.rss][RSS feed of the archives]]."
    ^ "\n\nIf you also wish to receive it every week by mail, you may subscribe to the"
    ^ " [[https://sympa.inria.fr/sympa/info/caml-list][caml-list]].\n\n");
  
  append out ("#+BEGIN_authorname\n[[https://alan.petitepomme.net/]"
    ^ "[Alan Schmitt]]\n#+END_authorname\n");

  (* End of function *)
  let oc = open_out filename in
  output_string oc !out;
  close_out oc

(* ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== *)

let export_as_rss cwn filename =
  (* Function to append things to the output *)
  let append sref s = sref := !sref ^ s in
  let out = ref ("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n") in

  (* Writing the titles of every entry into the item tag *)
  append out "<item>\n";
  let triple_of_date = function
    | [year; month; day] -> (day, month, year)
    | _ -> failwith "Wrong date format."
  in
  let (d, m, y) = String.split_on_char '.' cwn.date
    |> List.map int_of_string
    |> triple_of_date
  in
  let month_of_int = function
    | 1 -> "Jan"
    | 2 -> "Feb"
    | 3 -> "Mar"
    | 4 -> "Apr"
    | 5 -> "May"
    | 6 -> "Jun"
    | 7 -> "Jul"
    | 8 -> "Aug"
    | 9 -> "Sep"
    | 10 -> "Oct"
    | 11 -> "Nov"
    | 12 -> "Dec"
    | _ -> failwith "Impossible month."
  in
  let formatted_date = Printf.sprintf "%.2d %s %d" d (month_of_int m) y in

  append out ("  <title>OCaml Weekly News, " ^ formatted_date ^ "</title>\n"
    ^ "  <pubDate>" ^ formatted_date ^ " 12:00 GMT</pubDate>\n"
    ^ "  <link>https://alan.petitepomme.net/cwn/" ^ cwn.date ^ ".html</link>\n"
    ^ "  <guid>https://alan.petitepomme.net/cwn/" ^ cwn.date ^ ".html</guid>\n"
    ^ "  <description>&lt;ol&gt;");

  let write_entry_title i entry =
    append out (Printf.sprintf
      "&lt;li&gt;&lt;a href=\"https://alan.petitepomme.net/cwn/%s.html#%d\"&gt;"
      cwn.date (i+1));
    append out (Printf.sprintf "%s&lt;/a&gt;&lt;/li&gt;" entry.title);
  in
  List.iteri write_entry_title cwn.entries;
  
  append out "&lt;/ol&gt;</description>\n</item>";

  (* End of function *)
  let oc = open_out filename in
  output_string oc !out;
  close_out oc
