let without_extension s =
  let r = Str.regexp "\\." in
  let i =
    try Str.search_backward r s (String.length s - 1)
    with Not_found -> String.length s
  in
  String.sub s 0 i

let () =
  match Array.length Sys.argv with
  | 2 ->
    let cwn = Sys.argv.(1)
      |> Xmltree.to_clean_xmltree
      |> Cwn.to_cwn
    in
    let output_filename = without_extension Sys.argv.(1) in
    Cwn.export_as_orgmode cwn (output_filename ^ ".org");
    Cwn.export_as_rss cwn (output_filename ^ ".rss")
  | _ -> failwith ("Usage : " ^ Sys.argv.(0) ^ " filename.")