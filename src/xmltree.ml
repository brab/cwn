type xmltree =
  | Element of string * xmltree list
  | Data of string

(* ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== *)

let to_source filename =
  let ic = open_in filename in
  `Channel ic

let to_input source = Xmlm.make_input source

let to_xmltree input =
  let el ((_, tag_local), _) children = Element(tag_local, children) in
  let data d = Data d in
  snd (Xmlm.input_doc_tree ~el ~data input)

(* ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== *)

let get_children_with_tag tag = function
  | Element(_, c) ->
    let rec f = function
    | Element(t, cc) :: r -> if t = tag then cc else f r
    | Data _ :: r -> f r
    | [] -> failwith ("No element with tag \"" ^ tag ^ "\" in the document.")
    in f c
  | Data _ -> failwith "No children found."

let get_data_with_tag tag = function
  | Element(t, [Data d]) :: r ->
    if t = tag then (d, r)
    else failwith ("Wrong tag on first child : expected \""
      ^ tag ^ "\", found \"" ^ t ^ "\".")
  | [] -> failwith "No children found."
  | _ -> failwith "The first child has no tag."

let get_children = function
  | Element(_, c) -> c
  | Data _ -> []

let sanitise xmltree =
  let rgx = Str.regexp "^\n *$" in
  let rec cleanlist = function
    | [] -> []
    | (Data d) :: r ->
      if Str.string_match rgx d 0 then cleanlist r
      else (Data d) :: (cleanlist r)
    | Element(t, c) :: r ->
      let cleaned_c = cleanlist c in
      (Element(t, cleaned_c)) :: (cleanlist r)
  in
  match xmltree with
  | Data _ -> xmltree
  | Element(t, c) ->
    let cleaned_c = cleanlist c in Element(t, cleaned_c)

(* ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== *)

let to_clean_xmltree filename =
  filename |> to_source |> to_input |> to_xmltree |> sanitise